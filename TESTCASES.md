# Test Cases for Room 5

## Given that we are looking at August 2025

### Basic highlight rendering
1. If there are no registrations for the month
   1. it should not highlight any days.
   1. it should render number of available days as 31.
1. If there is 1 Room 5 registration for 1 day (eg: start_date: 20, end_date: 21)
   1. it should highlight the 20th on the calendar.
   1. it should render number of available days as 30.
   1. hovering over highlighted day should show the name of the registree
1. If there is 1 Room 5 registration for 2 days (eg: start_date: 20, end_date: 22)
   1. it should highlight the 20th and 21st on the calendar.
   1. it should render number of available days as 29.
   1. hovering over either highlighted day should show the name of the registree

### Non Room 5 handling
1. If there is 1 Room 6 registration
   1. it should not highlight any days.
   1. it should render number of available days as 31.
1. If there is both 1 Room 5 and 1 Room 6 registration
   1. it should highlight the room 5 day booking on the calendar only.
   1. it should render number of available days as 30.

### Exceptional Case
1. If there are 2 Room 5 registrations on the same day
   1. it should highlight one day and signal a conflict (Q: does the api allow this?).

### Pending/Reserved Interaction
1. If there is one pending Room 5 registration, and one reserved Room 5 registration (non conflicting)
   1. and if the show-pending switch is in the off position
      1. it should show the reserved highlight only.
      1. it should render number of available days as 30.
   1. and if the show-pending switch is in the on position
      1. it should show the pending highlight only.
      1. it should render number of available days as 29.
1. If there is one pending Room 5 registration, and one reserved Room 5 registration (same day)
   1. and if the show-pending switch is in the off position
      1. it should show the reserved highlight only.
      1. it should render number of available days as 30.
   1. and if the show-pending switch is in the on position
      1. it should show the pending highlight only.
      1. it should render number of available days as 30.

### API access
1. While API request is in flight
   1. a loading indicator should be shown
1. If an API response is not 2xx
   1. a useful, actionable message should be presented to the user
