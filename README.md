# Retreat Availability Calendar Project

## The Solution

### Client

I used [React](https://reactjs.org) to build the Retreat Availability
Calendar Project.  All of the component code lives in
[/client/src](https://gitlab.com/rcy/room5/tree/master/client/src).
Two third party packages were used: [momentjs](https://momentjs.com)
and
[react-big-calendar](http://intljusticemission.github.io/react-big-calendar/examples/index.html).
A html5 switch component found at https://jsfiddle.net/cysCx/59/ was
adapted into a Switch react component.

The entry point is `src/index.js` which renders `src/App.js` onto the
page.  `src/Calendar.js` is pure UI component, which accounts for the
bulk of the code and itself depends on `react-big-calendar`.
`src/CalendarWithData.js` is a wrapper component which is responsible
for fetching the data and passing it into the `Calendar` UI component.

A deployed version can be found at https://room5.surge.sh

### Server

I initially put together a [basic server in
Express](https://gitlab.com/rcy/room5/tree/master/server) with the
thought of hiding the Retreat Guru API and token from the client
directly, and having the client html and js served from express and
making api calls to `/api/registrations`.  The server makes requests
to the `demo14.secure.retreat.guru` endpoint and passes the response
directly back.

The deployed client version doesn't bother using this server and just
makes calls directly to the API endpoint from the client, but it would
just be a matter of deploying the server and
[updating the client](https://gitlab.com/rcy/room5/blob/master/client/src/CalendarWithData.js#L21)
to point to `/api/registrations`.

### Test Cases

There is a seperate document that describes [tests I would write for
the frontend](https://gitlab.com/rcy/room5/blob/master/TESTCASES.md).

### Local Installation

The development environment depends on `git`, `nodejs`, `yarn` and
`make`.  With those installed, the following steps will bring up a
local instance:

```
git clone https://gitlab.com/rcy/room5.git
cd room5
make
# should open default browser to http://localhost:3000
```

## The Problem

A spiritual retreat center needs to see the availability of one of their rooms (Room 5) for the month of August, 2025. They want you to build a page with the following criteria:

1. The page must contain a calendar for August 2025. Here's a **wireframe** provided by the center:

    [](https://www.notion.so/70e6bb2b8593473bb6f31b832f9a8ef5#f310bcdcee784df3b36459aaaab86e01)

1. When you hover over the day, show the name of the participant.
2. Any day where "Room 5" is occupied should be highlighted. 
3. Include a switch that, when on shows only registrations in the pending state. When it's turned off, the calendar goes back to its original state. 
4. Display the number of available days for the month at the bottom of the calendar
5. Create a separate file that lists each test description you would write for this project (for example, "if there are no registrations for the month, no days are highlighted")
6. Bonus: deploy your code to a cloud platform so that we can try your page out

## Additional Requirements

- The center uses Retreat Booking Guru, so you must use our public API to get registration data (see connection info below)
- You should use PHP and JavaScript to implement the solution, however if you are not familiar with PHP you can use any language. You can use any stack, library or framework.
- Once completed, send a link to your source code on Github, Gitlab, or Bitbucket etc to deryk@retreat.guru

## Connection information

- API documentation: [https://demo14.secure.retreat.guru/api](https://demo14.secure.retreat.guru/api)
- Registration endpoint: [https://demo14.secure.retreat.guru/api/v1/registrations?token=ef061e1a717568ee5ca5c76a94cf5842](https://demo14.secure.retreat.guru/api/v1/registrations?token=ef061e1a717568ee5ca5c76a94cf5842)


