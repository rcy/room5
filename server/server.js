const fetch = require('node-fetch');
const express = require('express')
const app = express()
const port = 3001

app.get('/api/:resource', (req, res) => {
  const apiBase = 'https://demo14.secure.retreat.guru/api/v1'
  const token = 'ef061e1a717568ee5ca5c76a94cf5842'

  const url = `${apiBase}/${req.params.resource}?token=${token}`

  fetch(url)
    .then(res => {
      console.log(`${res.status}: ${res.url}`)
      return res
    })
    .then(res => res.json())
    .then(json => res.send(json))
})


app.listen(port, () => console.log(`Example app listening on port ${port}!`))
