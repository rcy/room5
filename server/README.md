# Room 5 Server

In addition to serving static content (ie the single page frontend app
html/js/css), this server is responsible for proxying API requests
from the client to the retreat.guru API endpoint.

Client requests of the form:

```
/ENDPOINT
```

result in the server posting requests of the form:

```
https://demo14.secure.retreat.guru/api/v1/ENDPOINT?token=TOKEN
```

And returning the results unchanged to the client.

This protects the TOKEN from public view and allows configuration
changes of the API endpoint on the server to not affect the client.
