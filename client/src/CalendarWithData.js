import React from 'react'
import Calendar from './Calendar'

export default class CalendarWithData extends React.Component {
  state = { registrations: [] }

  registrationsToEvents(registrations) {
    return registrations
      .filter(reg => reg.room_id === this.props.roomId)
      .map(reg => ({
        title: reg.full_name,
        start: reg.start_date,
        end: reg.end_date,
        status: reg.status,
      }))
  }

  componentWillMount() {
    this.setState({ loading: true })

    return fetch('https://demo14.secure.retreat.guru/api/v1/registrations?token=ef061e1a717568ee5ca5c76a94cf5842')
      .then(res => res.json())
      .then(registrations => {
        this.setState({ registrations, loading: false })
      })
  }

  render() {
    return (
      <Calendar
        loading={this.state.loading}
        displayDate={this.props.displayDate}
        events={this.registrationsToEvents(this.state.registrations)}
      />
    )
  }
}
