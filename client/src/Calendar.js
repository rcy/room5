import PropTypes from 'prop-types'
import React from 'react'
import BigCalendar from 'react-big-calendar'
import Switch from './Switch'
import Moment from 'moment'
import { extendMoment } from 'moment-range'

import 'react-big-calendar/lib/css/react-big-calendar.css'
import './Calendar.css'

const moment = extendMoment(Moment)
const localizer = BigCalendar.momentLocalizer(moment)

export default class Calendar extends React.Component {
  static propTypes = {
    loading: PropTypes.bool,
    events: PropTypes.array,
  }

  state = {
    status: 'reserved'
  }

  dayPropGetter(date) {
    for (let e of this.filteredEvents(this.state.status)) {
      if (moment(date).isBefore(moment(e.end)) && moment(date).isAfter(moment(e.start).subtract(1, 'day'))) {
        return {
          className: `day day-${e.status}`,
        }
      }
    }
  }

  eventForCalendar(event) {
    return { ...event, start: moment(event.start), end: moment(event.end), allDay: true }
  }

  filteredEvents(status) {
    return this.props.events
               .map(event => this.eventForCalendar(event))
               .filter(event => !status || event.status === status)
  }

  togglePending(ev) {
    this.setState({ status: this.state.status === 'pending' ? 'reserved' : 'pending' })
  }

  eventRanges(status) {
    return this.filteredEvents(status).map(e => moment.range(e.start, moment(e.end).subtract(1, 'day')))
  }


  available() {
    const start = moment(this.props.displayDate)
    const end = moment(start).add(1, 'month').subtract(1, 'day')
    const range = moment.range(start, end)

    const eventRanges = this.eventRanges(this.state.status === 'reserved' ? 'reserved' : null)

    let available = range.diff('days') + 1

    for (let day of range.by('day')) {
      for (let er of eventRanges) {
        if (er.contains(day)) {
          available--
          break
        }
      }
    }
    return available;
  }

  renderHeader() {
    return (
      <div>
        <h1 style={{textAlign: 'center'}}>
          {moment(this.props.displayDate).format('MMMM YYYY')}
        </h1>
        <Switch
          label="Show all pending registrations"
          checked={this.state.status === 'pending' }
          onChange={() => this.togglePending() }
        />
      </div>
    )
  }

  render() {
    return (
      <div style={{height: '100%'}}>
        {this.renderHeader()}
        <BigCalendar
          localizer={localizer}
          defaultDate={moment(this.props.displayDate).toDate()}
          events={this.filteredEvents(this.state.status)}
          dayPropGetter={this.dayPropGetter.bind(this)}
          views={['month']}
          toolbar={false}
          components={{
            event: Event,
          }}
        />
        <div style={{ float: 'right' }}>
          <a href="https://gitlab.com/rcy/room5/blob/master/README.md">gitlab.com/rcy/room5</a>
        </div>
        {this.props.loading ? (
           <div>Loading...</div>
        ) : (
           <div>{this.available()} available days</div>
        )}
      </div>
    )
  }
}

class Event extends React.Component {
  render() {
    return <div style={{ height: '200px'}}></div>
  }
}
