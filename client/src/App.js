import React, { Component } from 'react'
import './App.css'
import CalendarWithData from './CalendarWithData'

class App extends Component {
  render() {
    return (
      <div className="app">
        <CalendarWithData
          roomId={6}
          displayDate={'2025-08-01'}
        />
      </div>
    )
  }
}

export default App
