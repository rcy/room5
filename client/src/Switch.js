/* switch component adapted from: https://jsfiddle.net/cysCx/59/ */

import PropTypes from 'prop-types'
import React from 'react'
import './switch.css'

export default class Switch extends React.Component {
  static propTypes = {
    checked: PropTypes.bool,
    label: PropTypes.string,
    onChange: PropTypes.func,
  }

  static lastId = 0

  componentWillMount() {
    this.id = `switchid-${++Switch.lastId}`
  }

  render() {
    return (
      <div>
        <input
          type="checkbox"
          id={this.id}
          name={this.id}
          className="switch"
          checked={this.props.checked}
          onChange={this.props.onChange}
        />
        <label htmlFor={this.id}>{this.props.label}</label>
        <div className="clear">&nbsp;</div>
      </div>
    )
  }
}
